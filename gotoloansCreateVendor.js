﻿describe('GTL Test ', function() {
  var width = 1280;
  var height = 980;
  var currentUrl;
  browser.driver.manage().window().setSize(width, height);
  it('create vendor ', function() {
    browser.waitForAngularEnabled(false);
      browser.get('https://dev-tandztech-gotoloans.tekstackapps.com/register');
      browser.sleep(1000);
 
    //first name
    element(by.id('mat-input-0')).sendKeys('Tim');
    browser.sleep(3000);
    //last name
    element(by.id('mat-input-1')).sendKeys('chen');
    browser.sleep(3000);
    //position
    element(by.id('mat-input-2')).sendKeys('CEO');
    browser.sleep(3000);
    //email
    element(by.id('mat-input-3')).sendKeys('vendor32@gmail.com');
    browser.sleep(3000);
    //phone
    element(by.id('mat-input-4')).sendKeys('6472234223');
    browser.sleep(3000);
    //password
    element(by.id('mat-input-6')).sendKeys('Admin1@#$');
    browser.sleep(3000);
    //continue
    element(by.xpath('//*[@id="supported-browsers"]/contact-information/div/div/gtlc-card/mat-card/contact-information-form/form/div[6]/gtlc-button/button/span')).click();
    browser.sleep(6000);
    //scroll down
    element(by.xpath('//*[@id="mat-dialog-0"]/terms-of-use-dialog/gtlc-dialog/div[2]/terms-of-use/gtlc-dialog/div[2]/div/div/div/div[25]/p[1]/strong')).click();
    browser.sleep(9000);
    //continue
    element(by.xpath('//*[@id="mat-dialog-0"]/terms-of-use-dialog/gtlc-dialog/div[2]/terms-of-use/gtlc-dialog/mat-dialog-actions/footer/div[1]/div[2]/gtlc-button/button/span')).click();
    browser.sleep(9000);
    element(by.id('mat-input-7')).sendKeys('MJK');
    browser.sleep(3000);
    element(by.id('mat-input-8')).sendKeys('M T K');
    browser.sleep(3000);
    element(by.css('[placeholder="Corporate Mailing Address"]')).sendKeys('1287 Queen Street West, Toronto, ON, Canada');
    browser.sleep(3000);
    element.all(by.className('pac-item')).get(0).click(); 
    element(by.id('mat-input-10')).click();
    element(by.id('mat-input-10')).sendKeys('M6K 1L6');
    browser.sleep(3000);
    element(by.id('mat-input-12')).sendKeys('9054367890');
    browser.sleep(3000);
    //continue
    element(by.xpath('//*[@id="cdk-step-content-0-0"]/business-profile/div/gtlc-card/mat-card/vendor-business-profile-form/form/gtlc-button/button/span')).click();
    browser.sleep(9000);
    //skip
    //element(by.xpath('//*[@id="cdk-step-content-0-1"]/business-logo/div/gtlc-card/mat-card/div[2]/button/span')).click();
    //browser.sleep(4000);

    var path = require('path');
    var fileToUpload = '/Users/bolajihamzat/Desktop/bf1cfa56-1dc6-4b5f-9705-5802fa2910d1.jpg';
    var absolutePath = path.resolve(fileToUpload);
    //upload1
    element.all(by.css('input[type="file"]')).then(function(items) {
          items[0].sendKeys(absolutePath);
        });
    browser.sleep(3000);
    element(by.xpath('//*[@id="cdk-step-content-0-1"]/business-logo/div/gtlc-card/mat-card/gtlc-button/button/span')).click();
    browser.sleep(3000);
    //upload2
    element.all(by.css('input[type="file"]')).then(function(items) {
          items[1].sendKeys(absolutePath);
        });
    browser.sleep(3000);
    element(by.xpath('//*[@id="cdk-step-content-0-2"]/verify-storefront/div/gtlc-card/mat-card/gtlc-button/button/span')).click();
    browser.sleep(3000);
    //upload3
    element(by.css('[placeholder="HST Number"]')).sendKeys('34567777');
    browser.sleep(3000);
    element.all(by.css('input[type="file"]')).then(function(items) {
          items[2].sendKeys(absolutePath);
        });
    browser.sleep(3000);
    //upload4
    element.all(by.css('input[type="file"]')).then(function(items) {
          items[3].sendKeys(absolutePath);
        });
    browser.sleep(3000);
    //continue
    element(by.xpath('//*[@id="cdk-step-content-0-3"]/banking-info/div/gtlc-card/mat-card/form/gtlc-button/button/span')).click();
    browser.sleep(3000);

});

});