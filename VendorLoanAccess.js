var path = require('path');
describe("We will access all vendor tabs",function()
{
	browser.manage().window().setSize(1600, 1000);
	var width = 1370;
	var height = 768;
	
	it("we are trying to reach loan details page" , function()
	{	
		browser.waitForAngularEnabled(false);
		browser.get("https://dev-tandztech-gotoloans.tekstackapps.com/vendor/signin");
		browser.driver.manage().window().setSize(width, height);
		browser.sleep(2000);
		element(by.css("#mat-input-0")).click().sendKeys("vendoradmin@tekstack.ca");
		element(by.css("#mat-input-1")).click().sendKeys("Admin1@#$");
		element(by.buttonText("Login")).click();
		browser.sleep(7000);
		element.all(by.css(".nav-item")).get(2).click();
		//element(by.css('fuse-nav-vertical-item:nth-of-type(2)>a>span')).click();
		browser.sleep(3000);
		element(by.id("form-field-status")).click();
		browser.sleep(2000);
		element.all(by.css("#mat-option-15")).click();
		//here you can enter text in searh filter
		element(by.css('input[name="filter"]')).click().sendKeys("2018 Alfa");
		//Click on your loan application
		browser.sleep(4000);
		element.all(by.css("datatable-body-cell")).get(3).click();
		browser.sleep(3000);
		element(by.css(".profile-info")).getText().then(function(text)
		{
			console.log(text);
			expect(text).toEqual("Black B Panther\n(783) 779-8124\nCan receive SMS / texts\nprayforme_the_weekend@gmaill.com\nDOB: 1994-01-25\n101 Treeline Boulevard, ON L3R4T5");
		});
		element(by.css(".employer")).getText().then(function(text)
		{
			console.log(text);
			expect(text).toEqual("Self-employed");
		});
		//element(by.css(".car-info")).getText().then(function(text)
		//{
			//console.log(text);
			//expect(text).toEqual("Self-employed");
		//});
		element(by.css(".active-application")).getText().then(function(text)
		{
			console.log(text);
			expect(text).toEqual("Application In Progress\nExpires: 2018-10-23\nVendor: Sahil Thapar\n(647) 821-3279\nAmount Requested: $2,500.00\nMax credit available: $10,000.00\nTerm Chosen: 36 Months\nFrequency: Bi-Weekly\nPayment : $51.02/Bi-Weekly\nUPDATE PAYMENT OPTIONS");
		});
		// Upload all documents to vendor checklists
	});
    var fileToUpload1 = 'C:/Users/HP/Desktop/Sahil office work/Front.png';
    var fileToUpload2 = 'C:/Users/HP/Desktop/Sahil office work/back.png';
    var fileToUpload3 = 'C:/Users/HP/Desktop/Sahil office work/left.jpg';
    var fileToUpload4 = 'C:/Users/HP/Desktop/Sahil office work/right.jpg';
    var fileToUpload5 = 'C:/Users/HP/Desktop/Sahil office work/Vehicle VIN Image.jpg';
    var fileToUpload6 = 'C:/Users/HP/Desktop/Sahil office work/Vehicle Odometer.jpg';
    var fileToUpload7 = 'C:/Users/HP/Desktop/Sahil office work/Test_Three.png';
    var fileToUpload8 = 'C:/Users/HP/Desktop/Sahil office work/Vehicle Repair Invoice.png';
	it('Upload File', function()
	{
		element(by.css(".details div:nth-child(2) div:nth-child(1) .wizard-button")).click();
		browser.sleep(2000);
		//Vehicle Body Images Upload 
		browser.sleep(3000);
		var absolutePath1 = path.resolve(__dirname, fileToUpload1);
		element.all(by.css('input[type="file"]')).then (function(items1)
		{
			browser.sleep(3000);
			items1[0].sendKeys(absolutePath1);
		});
		browser.sleep(3000);
		var absolutePath2 = path.resolve(__dirname, fileToUpload2);
		element.all(by.css('input[type="file"]')).then (function(items2)
		{
			browser.sleep(3000);
			items2[0].sendKeys(absolutePath2);
		});
		browser.sleep(3000);
		var absolutePath3 = path.resolve(__dirname, fileToUpload3);
		element.all(by.css('input[type="file"]')).then (function(items3)
		{
			browser.sleep(3000);
			items3[0].sendKeys(absolutePath3);
		});
		browser.sleep(3000);
		var absolutePath4 = path.resolve(__dirname, fileToUpload4);
		element.all(by.css('input[type="file"]')).then (function(items4)
		{
			browser.sleep(3000);
			items4[0].sendKeys(absolutePath4);
		});
		browser.sleep(2000);
		element(by.css(".controls button:nth-child(2)")).click();
		browser.sleep(4000);
		element(by.css(".controls button:nth-child(1)")).click();
		//Vehicle VIN Image Upload
		browser.sleep(3000);
		var absolutePath5 = path.resolve(__dirname, fileToUpload5);
		element.all(by.css('input[type="file"]')).then (function(items5)
		{
			browser.sleep(3000);
			items5[0].sendKeys(absolutePath5);
		});
		browser.sleep(3000);
		element(by.css(".controls button:nth-child(2)")).click();
		browser.sleep(4000);
		element(by.css("mat-horizontal-stepper mat-step-header:nth-child(5)")).click();
		//Vehicle Odometer Upload
		browser.sleep(3000);
		var absolutePath6 = path.resolve(__dirname, fileToUpload6);
		element.all(by.css('input[type="file"]')).then (function(items6)
		{
			browser.sleep(3000);
			items6[0].sendKeys(absolutePath6);
		});
		browser.sleep(3000);
		element(by.css(".controls button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.css("mat-horizontal-stepper mat-step-header:nth-child(7)")).click();
		//Vehicle Key Image Upload
		browser.sleep(3000);
		var absolutePath7 = path.resolve(__dirname, fileToUpload7);
		element.all(by.css('input[type="file"]')).then (function(items7)
		{
			browser.sleep(5000);
			items7[0].sendKeys(absolutePath7);
		});
		browser.sleep(5000);
		element(by.css(".controls button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.css("mat-horizontal-stepper mat-step-header:nth-child(9)")).click();
		//Repair Invoice Upload
		browser.sleep(3000);
		var absolutePath8 = path.resolve(__dirname, fileToUpload8);
		element.all(by.css('input[type="file"]')).then (function(items8)
		{
			browser.sleep(3000);
			items8[0].sendKeys(absolutePath8);
		});
		browser.sleep(3000);
		element(by.css(".controls button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.css(".header mat-icon:nth-child(2)")).click();
		browser.sleep(2000);		
	});
	browser.sleep(3000);
	
	/*
	it("we are trying to reach profile page" , function()
	{	
		/*element.all(by.css(".nav-item")).get(1).click();
		//element(by.css('fuse-nav-vertical-item:nth-of-type(2)>a>span')).click();
		browser.sleep(3000);
		element(by.id("mat-tab-label-0-1")).click();
		browser.sleep(2000);
		var accntnumber;
		element(by.id("form-field-accountNumber")).getText().then(function(text)
		{
			accntnumber = text
		}).then(function(){
			expect(accntnumber).toEqual('12345555');
		})
		console.log(accntnumber);
		element(by.id("form-field-institution")).clear().sendKeys("125");
		element(by.id("form-field-hstNumber")).clear().sendKeys("4546794137925");
		element(by.id("form-field-bankName")).clear().sendKeys("TD Banks");
		element(by.id("form-field-transitNumber")).clear().sendKeys("24587");
		element(by.id("btn-submit")).click();
		browser.sleep(3000);
		element(by.id("mat-tab-label-0-0")).click();
		browser.sleep(2000);
		element(by.id("mat-input-3")).clear().sendKeys("Sahil Thapar");
		element(by.id("mat-input-4")).clear().sendKeys("Thapar Corp.");
		element(by.id("mat-input-10")).clear().sendKeys("6478213279");
		element(by.id("mat-input-11")).clear().sendKeys("4168795623");
		element(by.id("btn-submit")).click();
		browser.sleep(3000);
		//element(by.id(".upload-clear")).click();
		//element(by.id("img-store-front")).click();
		//browser.sleep(3000);
		//here you can enter text in searh filter 
	});
	
	it("we are trying to reach home page" , function()
	{	
		element.all(by.css(".nav-item")).get(0).click();
		//element(by.css('fuse-nav-vertical-item:nth-of-type(2)>a>span')).click();
		browser.sleep(3000);
		element.all(by.css(".mat-select-value")).get(0).click();
		element.all(by.css("mat-option")).get(2).click();
		browser.sleep(2000);
		// First card APPLICATION IN PROGRESS
		element.all(by.css("gtl-dashboard-card:nth-child(1) mat-card:nth-child(1) div:nth-child(2)")).getText().then(function(text)
		{
			console.log(text);
			expect(text).toEqual([ '','','$106,658.43' ]);
		});
		element.all(by.css("gtl-dashboard-card:nth-child(1) mat-card:nth-child(1) div:nth-child(3)")).getText().then(function(text)
		{
			console.log(text);
			expect(text).toEqual([ '','Requests: (19)' ]);
		});
		
		browser.sleep(2000);
		// Second card READY TO SIGN
		element.all(by.css(".mat-select-value")).get(1).click();
		element.all(by.css("mat-option")).get(3).click();
		browser.sleep(2000);
		element.all(by.css("gtl-dashboard-card:nth-child(2) mat-card:nth-child(1) div:nth-child(2)")).getText().then(function(text)
		{
			console.log(text);
			expect(text).toEqual([ '','','$50,639.55' ]);
		});
		element.all(by.css("gtl-dashboard-card:nth-child(2) mat-card:nth-child(1) div:nth-child(3)")).getText().then(function(text)
		{
			console.log(text);
			expect(text).toEqual([ '','Requests: (13)' ]);
		});
		browser.sleep(2000);
		// Third card LOANS ISSUED
		element.all(by.css(".mat-select-value")).get(2).click();
		element.all(by.css("mat-option")).get(1).click();
		browser.sleep(2000);
		element.all(by.css("gtl-dashboard-card:nth-child(3) mat-card:nth-child(1) div:nth-child(2)")).getText().then(function(text)
		{
			console.log(text);
			expect(text).toEqual([ '','','$0.00' ]);
		});
		element.all(by.css("gtl-dashboard-card:nth-child(3) mat-card:nth-child(1) div:nth-child(3)")).getText().then(function(text)
		{
			console.log(text);
			expect(text).toEqual([ '','Requests: (0)' ]);
		});
		//element("datatable-row-wrapper:nth-child(5) .datatable-row-center").click();
		browser.sleep(2000);
	});*/
});