describe('GTL Test ', function() {
  var width = 1280;
  var height = 980;
  var message = 'Passed';
  var CXemail = 'GreenWorld69@UN.ca';
  browser.driver.manage().window().setSize(width, height);
  browser.waitForAngularEnabled(false);
 

    it('log in as customer and upload image', function() {
      browser.get('https://dev-tandztech-gotoloans.tekstackapps.com/');
      browser.sleep(3000);
      // Click on apply now
        var button= element(by.xpath('/html/body/app-root/home/section/div/div/div[1]/div[2]/button[2]'));

        
      button.click();
      browser.sleep(3000);
  // Selection Year, Make, Model and trim
      element(by.xpath("//span[. = 'Year']")).click();
      browser.sleep(1000);
      element(by.xpath("//span[. = '2017']")).click();
      browser.sleep(1000);
      element(by.xpath("//span[. = 'Make']")).click();
      browser.sleep(1000);
      element(by.xpath("//span[. = 'Audi']")).click();
      browser.sleep(1000);
      element(by.xpath("//span[. = 'Model']")).click();
      browser.sleep(1000);
      element(by.xpath("//span[. = 'A5']")).click();
      browser.sleep(1000);
      element(by.xpath("//span[. = 'Trim Level']")).click();
      browser.sleep(1000);
      element(by.xpath("//span[. = 'Progressiv']")).click();
      browser.sleep(1000);
      element(by.xpath("//span[. = 'Vehicle Style']")).click();
      browser.sleep(1000);
      element(by.xpath("//span[. = '2D Coupe 6sp']")).click();
      browser.sleep(1000);
      // Click on the Continue Button
      element(by.xpath("//span[. = ' Continue ']")).click();

      browser.sleep(2000);
     
       // Enter Kilometres and email
       browser.sleep(3000);  
       element(By.id('mat-input-1')).sendKeys('45678');
       browser.sleep(1000);
       //element(By.id('mat-input-1')).sendKeys(CXemail);
       browser.sleep(1000);
       element(by.xpath('/html/body/app-root/app-apply-now/car-additionals/div/div/gtlc-card/mat-card/car-additionals-form/form/app-navigation-buttons/gtlc-button[1]/button/span')).click();
       browser.sleep(9000);
       element(by.xpath('//*[@id="mat-dialog-0"]/submitting-loan-dialog/gtlc-dialog/mat-dialog-actions/footer/gtlc-button/button/span')).click();  
       browser.sleep(9000);
       element(by.xpath('//*[@id="supported-browsers"]/app-apply-now/loan-approval/div/gtlc-card/mat-card/div/approved-loan-approval/section/div[2]/gtlc-button/button')).click(); 
       browser.sleep(9000);  
       //Setting the Loan amount,term and time 
       var WeeklyEst ="$52.03 Weekly";
       //Change LoanAmount
       var ele = element(by.xpath('//*[@id="supported-browsers"]/app-apply-now/set-payment-options/div/div/gtlc-card/mat-card/payment-options-form/payments-slider/form/div[1]/div[2]/gtlc-slider/mat-slider/div/div[3]/div[2]'))
       browser.actions().
       mouseMove(ele).
       mouseMove({x: 75, y: 0}).
       doubleClick().
       perform();
       browser.sleep(6000);
       //*[@id="supported-browsers"]/app-apply-now/set-payment-options/div/div/gtlc-card/mat-card/payment-options-form/div/h1
       var Weekly =element(by.xpath('//*[@id="supported-browsers"]/app-apply-now/set-payment-options/div/div/gtlc-card/mat-card/payment-options-form/div/h1')).getText();
       expect(Weekly).toEqual(WeeklyEst);
       // change term 
       var terme = element(by.xpath('//*[@id="supported-browsers"]/app-apply-now/set-payment-options/div/div/gtlc-card/mat-card/payment-options-form/payments-slider/form/div[2]/div[2]/gtlc-slider/mat-slider/div/div[3]/div[2]'))
       browser.actions().
       mouseMove(terme).
       mouseMove({x: 50, y: 0}).
       doubleClick().
       perform();
       browser.sleep(6000);
       //change payment term
       var paye = element(by.xpath('//*[@id="supported-browsers"]/app-apply-now/set-payment-options/div/div/gtlc-card/mat-card/payment-options-form/payments-slider/form/div[3]/div[2]/gtlc-slider/mat-slider/div/div[3]/div[2]'))
       browser.actions().
       mouseMove(paye).
       mouseMove({x: 50, y: 0}).
       doubleClick().
       perform();
       browser.sleep(6000);
       element(By.id('mat-input-2')).sendKeys(CXemail);
       //*[@id="supported-browsers"]/app-apply-now/set-payment-options/div/div/gtlc-card/mat-card/payment-options-form/form/gtlc-button/button
       
 
       //*[@id="mat-input-2"]
       element(by.xpath('//*[@id="supported-browsers"]/app-apply-now/set-payment-options/div/div/gtlc-card/mat-card/payment-options-form/form/gtlc-button/button')).click(); 
 
       browser.sleep(6000);
        console.log('Done')
   
   

  });
  
  it('Log into Mailtrap', function() {
    browser.get('https://mailtrap.io/');
    element(by.xpath("/html/body/header/div/div[2]/div/a[1]")).click();
    ////*[@id="user_password"]
    element(By.xpath('//*[@id="user_email"]')).sendKeys('sahil@translucentcomputing.com');
    element(By.xpath('//*[@id="user_password"]')).sendKeys('ct9780806446');
    element(By.xpath('//*[@id="new_user"]/div[4]/input')).click();
    browser.sleep(5000);
    //--------------------Click on shared Message-------------------------------------
    element(By.xpath('//*[@id="header"]/div/ul/li[1]/a')).click();
    browser.sleep(5000);
    element(By.xpath('//*[@id="main"]/div/div[2]/div[2]/table/tbody/tr[3]/td[1]/div[1]/strong/a')).click();
    //-------------------Search for Specific Messages----------------------------------
    browser.sleep(5000);
    element(By.xpath('//*[@id="main"]/div/div[1]/div/div[1]/div/div[2]/div/input')).sendKeys(CXemail);
    browser.sleep(5000);
    element(By.xpath('//*[@id="main"]/div/div[1]/div/ul/li')).click();
    browser.sleep(5000);
    element(By.xpath('/html/body/center/table[3]/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr/td/p[1]/a')).ngclick();
    browser.sleep(9000);
    browser.sleep(9000);
    browser.sleep(9000);
    browser.sleep(9000);
    browser.sleep(9000);
    });

});