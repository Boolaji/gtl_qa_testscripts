describe('GTL Test ', function() {
  var width = 1280;
  var height = 980;
  var message = 'Passed';
  browser.driver.manage().window().setSize(width, height);
 

    it('log in as customer and upload image', function() {
    browser.waitForAngularEnabled(false);
      browser.get('https://dev-tandztech-gotoloans.tekstackapps.com/');
      browser.sleep(3000);
      // Click on Sign in
      var button= element(by.xpath('//*[@id="supported-browsers"]/home/section/div/div/div[2]/make-model-box/div/a'));
      button.click();
      browser.sleep(3000);
      element(by.xpath('//*[@id="mat-input-1"]')).clear();
      element(by.xpath('//*[@id="mat-input-1"]')).sendKeys('susan.ww@tekstack.ca');
      browser.sleep(1000);
      element(by.xpath('//*[@id="mat-input-2"]')).sendKeys('Admin1@#$');
      element(by.xpath("//span[. = ' Login ']")).click();
      browser.sleep(8000);
      //already choose amount, just click save
      element(by.xpath("//span[. = ' SAVE & CONTINUE ']")).click();
      browser.sleep(3000);
      element(by.xpath("//span[. = 'Save & Continue']")).click();
      browser.sleep(3000);
      element(by.xpath("//span[. = 'Continue']")).click();
      browser.sleep(3000);
      // upload first file
        var path = require('path');
        var fileToUpload = '/Users/bolajihamzat/Desktop/bf1cfa56-1dc6-4b5f-9705-5802fa2910d1.jpg';
        var absolutePath = path.resolve(fileToUpload);

        element.all(by.css('input[type="file"]')).then(function(items) {
          items[0].sendKeys(absolutePath);
        });
        browser.sleep(6000);
      
     //enter date of birth
      element(by.id('mat-input-3')).sendKeys('1999');
      element(by.id('mat-input-4')).sendKeys('09');
      element(by.id('mat-input-5')).sendKeys('22');
      browser.sleep(3000);

      element(by.xpath('//*[@id="cdk-step-content-1-1"]/app-verification-id/gtlc-card/mat-card/identification-form/form/footer/gtlc-button/button/span')).click();

      browser.sleep(6000);
    
    //upload second file
      element.all(by.css('input[type="file"]')).then(function(items) {
          items[1].sendKeys(absolutePath);
      });
        
      browser.sleep(3000);
      element(by.xpath('//*[@id="cdk-step-content-1-2"]/app-verification-ownership/gtlc-card/mat-card/gtlc-button/button/span')).click();

      browser.sleep(6000);

    // upload third file
      element.all(by.css('input[type="file"]')).then(function(items) {
          items[2].sendKeys(absolutePath);
      });
        
      browser.sleep(3000);
      element(by.xpath('//*[@id="cdk-step-content-1-3"]/app-verification-insurance/gtlc-card/mat-card/gtlc-button/button/span')).click();

      browser.sleep(6000);
    //upload fourth file
    element.all(by.css('input[type="file"]')).then(function(items) {
          items[3].sendKeys(absolutePath);
      });
        
      browser.sleep(3000);
    //this is last step to finish upload, click this, customer email will be updated so i disable it.
      //element(by.xpath('//*[@id="cdk-step-content-1-4"]/app-verification-bank/gtlc-card/mat-card/gtlc-button/button/span')).click();

      browser.sleep(6000);

    


  });







});