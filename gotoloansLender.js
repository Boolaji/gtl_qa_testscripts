describe('GTL Test ', function() {
  var width = 1280;
  var height = 980;
  var currentUrl;
  browser.driver.manage().window().setSize(width, height);
  browser.waitForAngularEnabled(false);
  
//----------------------------------------------------------------------------------------------  
	
  it('Testing the Home page ', function() 
  { 	// Sign in to the App
		var message  ='Passed';
		//var homeUrl 
		var homeUrl= 'https://dev-tandztech-gotoloans.tekstackapps.com/admin/lender/home';
		browser.waitForAngularEnabled(false);
		element(by.id('username-input')).clear();
		element(by.id('username-input')).sendKeys('lenderadmin@tekstack.ca');
		element(by.id('password-input')).clear();
		element(by.id('password-input')).sendKeys('Admin1@#$');
		browser.sleep(3000);
		element(by.css("#signin-button")).click();
		browser.sleep(5000);
		currentUrl= browser.getCurrentUrl();
		expect(currentUrl).toEqual(homeUrl);
		//element(by.css('fuse-nav-vertical-item:nth-of-type(2)>a>span')).click();
		browser.sleep(3000);
		//element.all(by.css(".mat-select-value")).get(0).click();
		//element.all(by.css("mat-option")).get(2).click();
		browser.sleep(2000);
		// First card APPLICATION IN PROGRESS
		element.all(by.css("gtl-dashboard-card:nth-child(1) mat-card:nth-child(1) div:nth-child(2)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','','$106,658.43' ]);
		});
		element.all(by.css("gtl-dashboard-card:nth-child(1) mat-card:nth-child(1) div:nth-child(3)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','Requests: (19)' ]);
		});
		
		browser.sleep(2000);
		// Second card READY TO SIGN
		element.all(by.css(".mat-select-value")).get(1).click();
		element.all(by.css("mat-option")).get(3).click();
		browser.sleep(2000);
		element.all(by.css("gtl-dashboard-card:nth-child(2) mat-card:nth-child(1) div:nth-child(2)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','','$50,639.55' ]);
		});
		element.all(by.css("gtl-dashboard-card:nth-child(2) mat-card:nth-child(1) div:nth-child(3)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','Requests: (13)' ]);
		});
		browser.sleep(2000);
		// Third card LOANS ISSUED
		element.all(by.css(".mat-select-value")).get(2).click();
		element.all(by.css("mat-option")).get(1).click();
		browser.sleep(2000);
		element.all(by.css("gtl-dashboard-card:nth-child(3) mat-card:nth-child(1) div:nth-child(2)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','','$0.00' ]);
		});
		element.all(by.css("gtl-dashboard-card:nth-child(3) mat-card:nth-child(1) div:nth-child(3)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','Requests: (0)' ]);
		});
		//element("datatable-row-wrapper:nth-child(5) .datatable-row-center").click();
		browser.sleep(3000);
	});

	it('Testing Loans Tab ', function() 
	{
		// Loan Check list variables
		var loanUrl = "https://dev-tandztech-gotoloans.tekstackapps.com/admin/lender/loans";
		//var name , email ,Vehicle,phone;
		browser.sleep(3000);
		element(by.css('fuse-nav-vertical-item:nth-of-type(2)>a>span')).click();
		currentUrl= browser.getCurrentUrl();
		browser.sleep(3000);
		// console.log("The Url is :");
		expect(currentUrl).toEqual(loanUrl);
		browser.sleep(3000);
		element(by.id("form-field-status")).click();
		browser.sleep(2000);
		element.all(by.css("mat-option")).get(3).click();
		//here you can enter text in searh filter
		element(by.css('input[name="filter"]')).click().sendKeys("Teusday");
		//Click on your loan application
		browser.sleep(2000);
		element.all(by.css("datatable-body-cell")).get(3).click();
		browser.sleep(7000);
		// Here is time to validate the values selected in the previous screen
		element(by.css(".profile-info")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual("Black B Panther\n(783) 779-8124\nCan receive SMS / texts\nprayforme_the_weekend@gmaill.com\nDOB: 1994-01-25\n101 Treeline Boulevard, ON L3R4T5");
		});
		element(by.css(".employer")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual("Self-employed");
		});
		element(by.css(".details div:nth-child(1) div:nth-child(1) div:nth-child(1)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual("Self-employed");
		});
		element(by.css(".active-application")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual("Application In Progress\nExpires: 2018-10-23\nVendor: Sahil Thapar\n(647) 821-3279\nAmount Requested: $2,500.00\nMax credit available: $10,000.00\nTerm Chosen: 36 Months\nFrequency: Bi-Weekly\nPayment : $51.02/Bi-Weekly\nUPDATE PAYMENT OPTIONS");
		});
		
		browser.sleep(2000);
		// TESTING CHECKLIST ACTION SCREENS
		/*//CUSTOMER ID CHECKLIST
		element.all(by.css(".check-list-area section:nth-child(1) ul:nth-child(1) li:nth-child(1) button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.css(".closed form div textarea:nth-child(3)")).click().sendKeys("Not a proper ID proof");
		browser.sleep(3000);
		element(by.css(".closed form div div:nth-child(4) button:nth-child(2)")).click();
		browser.sleep(3000);
		//element(by.css(".header mat-icon:nth-child(2)"));
		browser.sleep(2000);
		// CUSTOMER BANKING CHECKLIST (id's keeps on changing everytime)
		element.all(by.css(".check-list-area section:nth-child(1) ul:nth-child(1) li:nth-child(2) button:nth-child(2)")).click();
		browser.sleep(3000);
		element.all(by.css("#left div:nth-child(2) div:nth-child(1) div:nth-child(3) mat-form-field:nth-child(1) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1) ")).click().clear().sendKeys("TD BANK");
		element.all(by.css("#left div:nth-child(2) div:nth-child(1) div:nth-child(3) mat-form-field:nth-child(2) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1) ")).click().clear().sendKeys("012");
		element.all(by.css("#left div:nth-child(2) div:nth-child(1) div:nth-child(4) mat-form-field:nth-child(1) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1) ")).click().clear().sendKeys("12547");
		element.all(by.css("#left div:nth-child(2) div:nth-child(1) div:nth-child(4) mat-form-field:nth-child(2) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1) ")).click().clear().sendKeys("QWERTY--786Z");
		element.all(by.css("#left div:nth-child(2) div:nth-child(1) div:nth-child(5) mat-form-field:nth-child(1) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1) ")).click().clear().sendKeys("355 Bloor Street East");
		element(by.css(".closed form div textarea:nth-child(2)")).click().sendKeys("ALL GOOD"); 
		element(by.css(".closed form div div:nth-child(4) button:nth-child(1)")).click();
		//element(by.css("#dialog div:nth-child(1) mat-icon:nth-child(2)"));
		browser.sleep(2000);*/		
		//CUSTOMER INSURANCE CHECKLIST (Element not clickable issue)
		element.all(by.css(".check-list-area section:nth-child(2) ul:nth-child(1) li:nth-child(2) button:nth-child(2)")).click();
		browser.sleep(4000);
		element.all(by.css("#left div:nth-child(2) div:nth-child(4) div:nth-child(3) div:nth-child(1) mat-form-field:nth-child(1) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1)")).click().clear().sendKeys("QWERTY COMPANY");
		element.all(by.css("#left div:nth-child(2) div:nth-child(4) div:nth-child(3) div:nth-child(1) mat-form-field:nth-child(2) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1)")).click().clear().sendKeys("14523698705");
		browser.sleep(2000);
		browser.actions().mouseMove(element.all(by.css("#left div:nth-child(2) div:nth-child(4) div:nth-child(3) div:nth-child(5) mat-form-field:nth-child(1) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1)"))).click();
		browser.sleep(2000);
		element.all(by.css("#left div:nth-child(2) div:nth-child(4) div:nth-child(3) div:nth-child(5) mat-form-field:nth-child(1) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1)")).click().clear().sendKeys("50 Gerrad Street");
		element.all(by.css("#left div:nth-child(2) div:nth-child(4) div:nth-child(3) div:nth-child(5) mat-form-field:nth-child(2) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1)")).click().clear().sendKeys("Toronto");
		element.all(by.css("#left div:nth-child(2) div:nth-child(4) div:nth-child(3) div:nth-child(6) mat-form-field:nth-child(1) div:nth-child(1) div:nth-child(1) div:nth-child(1) mat-select:nth-child(1)")).click();
		browser.sleep(2000);
		element.all(by.css("#cdk-overlay-3 div:nth-child(1) div:nth-child(1) mat-option:nth-child(7)")).click();
		element.all(by.css("#left div:nth-child(2) div:nth-child(4) div:nth-child(3) div:nth-child(6) mat-form-field:nth-child(2) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1)")).click().clear().sendKeys("M5Y 8U3");
		element(by.css(".closed form div div:nth-child(4) button:nth-child(3)")).click();
		element(by.css("#dialog div:nth-child(1) mat-icon:nth-child(2)"));
		browser.sleep(2000); 
		//CUSOMTER OWNERSHIP CHECKLIST
		element.all(by.css(".check-list-area section:nth-child(2) ul:nth-child(1) li:nth-child(3) button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.css(".closed form div textarea:nth-child(3)")).click().sendKeys("Not clear picture");
		browser.sleep(3000);
		element(by.css(".closed form div div:nth-child(4) button:nth-child(2)")).click();
		//element(by.css("#dialog div:nth-child(1) mat-icon:nth-child(2)"));
		//VEHICLE BODY IMAGE CHECKLIST
		element.all(by.css(".check-list-area section:nth-child(2) ul:nth-child(1) li:nth-child(4) button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.css(".closed form div div:nth-child(4) button:nth-child(1)")).click();
		browser.sleep(3000);
		//VEHICLE VIN IMAGE CHECKLIST (Element not clickable issue due to SAVE button in VVLA, its a BIG issue, RAJIV working on this one though. He was having same peoblem in CBB)
		element.all(by.css(".check-list-area section:nth-child(2) ul:nth-child(1) li:nth-child(5) button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.id("accept")).click();
		browser.sleep(2000);
		element(by.id("mat-input-7")).click().sendKeys("WD3BE8CC9D5748412");
		element(by.css("#left div:nth-child(1) div:nth-child(10) button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.css(".closed form div div:nth-child(4) button:nth-child(1)")).click();
		browser.sleep(3000);
		//VEHICLE ODOMETER CHECKLIST
		element.all(by.css(".check-list-area section:nth-child(2) ul:nth-child(1) li:nth-child(6) button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.css(".closed form div div:nth-child(4) button:nth-child(1)")).click();
		browser.sleep(3000);
		//VEHICLE KEY IMAGE CHECKLIST
		element.all(by.css(".check-list-area section:nth-child(2) ul:nth-child(1) li:nth-child(7) button:nth-child(2)")).click();
		browser.sleep(3000);
		element(by.css(".closed form div div:nth-child(4) button:nth-child(1)")).click();
		browser.sleep(3000);
		//REPAIR INVOICE CHECKLIST
		element.all(by.css(".check-list-area section:nth-child(2) ul:nth-child(1) li:nth-child(8) button:nth-child(2)")).click();
		browser.sleep(4000);
		element(by.css("#left div:nth-child(2) div:nth-child(1) div:nth-child(2) mat-form-field:nth-child(1) div:nth-child(1) div:nth-child(1) div:nth-child(1) input:nth-child(1)")).click().sendKeys("2000");
		browser.sleep(2000);
		element(by.css(".closed form div div:nth-child(4) button:nth-child(1)")).click();
		browser.sleep(3000);
	});


	/*it('Testing Vendors Tab  ', function() 
	{	
		var vendorUrl ="https://dev-tandztech-gotoloans.tekstackapps.com/admin/lender/vendors";
		var vendorName , vendorEmail,dateActivated,location,phone;
		// Open Vendors Tab
		element(by.css('fuse-nav-vertical-item:nth-of-type(3)>a>span')).click();
		browser.sleep(2000);
		currentUrl= browser.getCurrentUrl();
		// console.log("The Url is :");
		expect(currentUrl).toEqual(vendorUrl);
		//  open the first Vendor
		element(by.css('datatable-row-wrapper:nth-of-type(2)>datatable-body-row>div:nth-of-type(2)>datatable-body-cell:nth-of-type(3)')).click(); 
		//element(by.css('gtl-vendor-settings>div:nth-of-type(1)>div:nth-of-type(1)>div:nth-of-type(1)>mat-icon')).click();
	 	//click on the Update button  
		element(by.id('btn-submit')).click();
		browser.sleep(2000);
		//open the first Vendor
		element(by.css('datatable-row-wrapper:nth-of-type(1)>datatable-body-row>div:nth-of-type(2)>datatable-body-cell:nth-of-type(3)')).click();
		//Open the shop hours page  
		element(by.id('btn-navto-shop-hours')).click();
		element(by.id('btn-submit')).click();
		browser.sleep(1000);
		//Step Skipped because of a bug (GTL-750)   
		//click the Update button
		element(by.id('btn-submit')).click();
		element(by.css('button:nth-of-type(3)>span')).click(); 
		element(by.id('btn-navto-services-offered')).click();
		element(by.css('gtl-vendor-settings>div:nth-of-type(1)>div:nth-of-type(1)>div:nth-of-type(1)>mat-icon')).click();
		browser.sleep(2000);
		The rest of the Vendor Test Scripts would be kept here       
	});*/

	// To be coded when there is live data here 
	/** 
	it('Testing User Tab  ', function() 
	{
		var userUrl ="https://dev-tandztech-gotoloans.tekstackapps.com/admin/lender/users";
        browser.sleep(1000);
        // click the UserTab
        element(by.css('fuse-nav-vertical-item:nth-of-type(4)>a>span')).click();
        browser.sleep(1000);
        // Click the first available 
        element(by.css('datatable-body-cell:nth-of-type(1)>div>span')).click();
        //browser.sleep(1000);
        element(by.css('div:nth-of-type(61)>button:nth-of-type(2)')).click();
        browser.sleep(3000);
        //element(by.css('gtl-dialog>div:nth-of-type(1)>div:nth-of-type(1)>mat-icon')).click();
        currentUrl= browser.getCurrentUrl();
		// console.log("The Url is :");
		expect(currentUrl).toEqual(userUrl);
		element(by.css('div:nth-of-type(5)>button>span>div>img')).click();
		element(by.css('#logoutBtn')).click();
	});
	**/

	/*it('Testing Loans Pro Tab  ', function() 
	{
		var loanProUrl ="https://dev-tandztech-gotoloans.tekstackapps.com/admin/lender/loanpro";
		element(by.css('fuse-nav-vertical-item:nth-of-type(5)>a>span')).click();
		currentUrl = browser.getCurrentUrl();
		browser.sleep(1000);
		expect(loanProUrl).toEqual(currentUrl);
		// SIgn Out of the App
		//element(by.id('userMenuBtn')).click();
		//browser.sleep(1000);
		//element(by.id('logoutBtn')).click();
		//browser.sleep(5000);
	});*/
	/*it('Testing Settings Tab  ', function() 
	{
		
	});*/
});