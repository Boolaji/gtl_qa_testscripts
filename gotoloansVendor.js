var path = require('path');
describe('GTL Test ', function() {
  var width = 1370;
  var height = 768;
  var currentUrl;
  browser.driver.manage().window().setSize(width, height);
 
//----------------------------------------------------------------------------------------------  
 
it('Testing the Home page ', function() 
  {  
		browser.waitForAngularEnabled(false);
		browser.get("https://dev-tandztech-gotoloans.tekstackapps.com/vendor/signin");
		browser.driver.manage().window().setSize(width, height);
		browser.sleep(2000);
		// SIgn in to the App
		var message  ='Passed';
		//var homeUrl 
		var homeUrl= 'https://dev-tandztech-gotoloans.tekstackapps.com/admin/vendor/home';
		element(by.css("#mat-input-0")).clear().click().sendKeys('vendoradmin@tekstack.ca');
		element(by.css("#mat-input-1")).clear().click().sendKeys('Admin1@#$');
		element(by.buttonText("Login")).click();
		browser.sleep(7000);
		currentUrl= browser.getCurrentUrl();
		expect(currentUrl).toEqual(homeUrl);
		// Validate That It loads to the home Tab when user signs in
		// Test the Applicatin in progress Forms on the home page
		//element.all(by.css(".nav-item")).get(0).click();
		//element(by.css('fuse-nav-vertical-item:nth-of-type(2)>a>span')).click();
		// First card APPLICATION IN PROGRESS
		// this tests are USELESS 
		element.all(by.css(".mat-select-value")).get(0).click();
		element.all(by.css("mat-option")).get(2).click();
		browser.sleep(2000);
		element.all(by.css("gtl-dashboard-card:nth-child(1) mat-card:nth-child(1) div:nth-child(2)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','','$26,145.00' ]);
		});
		element.all(by.css("gtl-dashboard-card:nth-child(1) mat-card:nth-child(1) div:nth-child(3)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','Requests: (4)' ]);
		});
		
		browser.sleep(2000);
		// Second card READY TO SIGN
 		element.all(by.css(".mat-select-value")).get(1).click();
		element.all(by.css("mat-option")).get(3).click();
		browser.sleep(2000);
		element.all(by.css("gtl-dashboard-card:nth-child(2) mat-card:nth-child(1) div:nth-child(2)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','','$0.00' ]);
		});
		element.all(by.css("gtl-dashboard-card:nth-child(2) mat-card:nth-child(1) div:nth-child(3)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','Requests: (0)' ]);
		});
		browser.sleep(2000);
		// Third card LOANS ISSUED
		element.all(by.css(".mat-select-value")).get(2).click();
		element.all(by.css("mat-option")).get(1).click();
		browser.sleep(2000);
		element.all(by.css("gtl-dashboard-card:nth-child(3) mat-card:nth-child(1) div:nth-child(2)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','','$0.00' ]);
		});
		element.all(by.css("gtl-dashboard-card:nth-child(3) mat-card:nth-child(1) div:nth-child(3)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual([ '','Requests: (0)' ]);
		});
		//element("datatable-row-wrapper:nth-child(5) .datatable-row-center").click();
		browser.sleep(2000);
		
   });

it('Testing Loans Tab ', function() { 
		// Loan Check list variables
		browser.waitForAngularEnabled(false);
		var loanUrl = "https://dev-tandztech-gotoloans.tekstackapps.com/admin/vendor/loans";
		var name , email ,Vehicle,phone;
		browser.sleep(1000);
		//main-navigation
		//element(by.id('main-navigation')).click();
		//browser.driver.manage().window().setSize(width, height);
		element.all(by.css(".nav-item")).get(2).click();
		browser.sleep(1000);
		currentUrl= browser.getCurrentUrl();
		// console.log("The Url is :");
		expect(currentUrl).toEqual(loanUrl);
		//element(by.css('fuse-nav-vertical-item:nth-of-type(2)>a>span')).click();
		browser.sleep(3000);
		element(by.id("form-field-status")).click();
		browser.sleep(2000);
		element.all(by.css("#mat-option-15")).click();
		browser.sleep(3000);
		//here you can enter text in searh filter
		element(by.css('input[name="filter"]')).click().sendKeys("babur");
		//Click on your loan application
		browser.sleep(2000);
		element.all(by.css("datatable-body-cell")).get(3).click();
		browser.sleep(10000);
		//Verifying customer profile section
		element(by.css(".profile-info")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual("Asd T As\n(123) 212-3211\nCan receive SMS / texts\nbass3@tekstack.ca\nDOB: 1990-10-10\nAddress, ON S1S1S1");
		});
		//verifying customer Employment section
		element(by.css(".employer")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual("Student");
		});
		//Verifying customer vehicle info.
		element(by.css(".details div:nth-child(1) div:nth-child(1) div:nth-child(1)")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual("Self-employed");
		});
		//Verifying Payment option section
		element(by.css(".active-application")).getText().then(function(text)
		{
			console.log(text);
			//expect(text).toEqual("Application In Progress\nExpires: 2018-10-30\nVendor: Sahil Thapar\n(123) 456-7891\nAmount Requested: $4,645.00\nMax credit available: $10,000.00\nTerm Chosen: 24 Months\nFrequency: Weekly\nPayment : $60.86/Weekly\nUPDATE PAYMENT OPTIONS");
		});
		
		
		

});
//VENDOR UPLOADS
	var fileToUpload1 = '/Users/bolajihamzat/gtl_qa_testscripts/second.jpg';
	var fileToUpload2 = '/Users/bolajihamzat/gtl_qa_testscripts/second.jpg';
	var fileToUpload3 = '/Users/bolajihamzat/gtl_qa_testscripts/second.jpg';
	var fileToUpload4 = '/Users/bolajihamzat/gtl_qa_testscripts/second.jpg';
	var fileToUpload5 = '/Users/bolajihamzat/gtl_qa_testscripts/second.jpg';
	var fileToUpload6 = '/Users/bolajihamzat/gtl_qa_testscripts/second.jpg';
	var fileToUpload7 = '/Users/bolajihamzat/gtl_qa_testscripts/second.jpg';
	var fileToUpload8 = '/Users/bolajihamzat/gtl_qa_testscripts/second.jpg';
//var fileToUpload1 = 'C:/Users/HP/Desktop/Sahil office work/Front.png';*/
it('Upload File', function()
	{
		element(by.css(".details div:nth-child(2) div:nth-child(1) .wizard-button")).click();
		browser.sleep(2000);
		//Vehicle Body Images Upload 
		browser.sleep(3000);
		var absolutePath1 = path.resolve(__dirname, fileToUpload1);
		element.all(by.css('input[type="file"]')).then (function(items1)
		{
			browser.sleep(3000);
			items1[0].sendKeys(absolutePath1);
		});
		browser.sleep(3000);
		var absolutePath2 = path.resolve(__dirname, fileToUpload2);
		element.all(by.css('input[type="file"]')).then (function(items2)
		{
			browser.sleep(3000);
			items2[0].sendKeys(absolutePath2);
		});
		browser.sleep(3000);
		var absolutePath3 = path.resolve(__dirname, fileToUpload3);
		element.all(by.css('input[type="file"]')).then (function(items3)
		{
			browser.sleep(3000);
			items3[0].sendKeys(absolutePath3);
		});
		browser.sleep(3000);
		var absolutePath4 = path.resolve(__dirname, fileToUpload4);
		element.all(by.css('input[type="file"]')).then (function(items4)
		{
			browser.sleep(3000);
			items4[0].sendKeys(absolutePath4);
		});
		browser.sleep(2000);
		element(by.css("#dialog div:nth-child(2) div:nth-child(1) mat-horizontal-stepper:nth-child(1) div:nth-child(2) div:nth-child(1) form:nth-child(1) div:nth-child(2) div:nth-child(3) button:nth-child(2)")).click();
		//browser.sleep(5000);
		//element(by.css("#dialog div:nth-child(2) div:nth-child(1) mat-horizontal-stepper:nth-child(1) div:nth-child(1) mat-step-header:nth-child(3)")).click();
		
		//Vehicle VIN Image Upload
		browser.sleep(3000);
		var absolutePath5 = path.resolve(__dirname, fileToUpload5);
		element.all(by.css('input[type="file"]')).then (function(items5)
		{
			browser.sleep(3000);
			items5[0].sendKeys(absolutePath5);
		});
		browser.sleep(3000);
		element(by.css(".controls button:nth-child(2)")).click();
	    //browser.sleep(4000);
		//element(by.css("#dialog div:nth-child(2) div:nth-child(1) mat-horizontal-stepper:nth-child(1) div:nth-child(1) mat-step-header:nth-child(5)")).click();
		
		//Vehicle Odometer Upload
		browser.sleep(2000);
		var absolutePath6 = path.resolve(__dirname, fileToUpload6);
		element.all(by.css('input[type="file"]')).then (function(items6)
		{
			browser.sleep(3000);
			items6[0].sendKeys(absolutePath6);
		});
		browser.sleep(3000);
		element(by.css(".controls button:nth-child(2)")).click();
		//browser.sleep(6000);
		//element(by.css("#dialog div:nth-child(2) div:nth-child(1) mat-horizontal-stepper:nth-child(1) div:nth-child(1) mat-step-header:nth-child(7)")).click();
		//Vehicle Key Image Upload
		browser.sleep(3000);
		var absolutePath7 = path.resolve(__dirname, fileToUpload7);
		element.all(by.css('input[type="file"]')).then (function(items7)
		{
			browser.sleep(2000);
			items7[0].sendKeys(absolutePath7);
		});
		browser.sleep(4000);
		element(by.css(".controls button:nth-child(2)")).click();		
		//Repair Invoice Upload
		browser.sleep(3000);
		var absolutePath8 = path.resolve(__dirname, fileToUpload8);
		element.all(by.css('input[type="file"]')).then (function(items8)
		{
			browser.sleep(3000);
			items8[0].sendKeys(absolutePath8);
		});
		browser.sleep(3000);
		element(by.css(".controls button:nth-child(2)")).click();
		browser.sleep(4000);
		element(by.css("#dialog div:nth-child(1) mat-icon:nth-child(2)")).click();		
		browser.sleep(3000);

	});
/*
	it('Testing profile Tab  ', function() {
		var profUrl ="https://dev-tandztech-gotoloans.tekstackapps.com/admin/vendor/profile";
        browser.sleep(1000);
        // console.log("The Url is :");
		element.all(by.css(".nav-item")).get(1).click();
		//element(by.css('fuse-nav-vertical-item:nth-of-type(2)>a>span')).click();
		browser.sleep(3000);
		currentUrl= browser.getCurrentUrl();
		expect(currentUrl).toEqual(profUrl);
		element(by.id("mat-tab-label-0-1")).click();
		browser.sleep(2000);
		element(by.id("form-field-accountNumber")).clear().sendKeys("AA7894569");
		element(by.id("form-field-institution")).clear().sendKeys("125");
		element(by.id("form-field-hstNumber")).clear().sendKeys("4546794137925");
		element(by.id("form-field-bankName")).clear().sendKeys("TD Banks");
		element(by.id("form-field-transitNumber")).clear().sendKeys("24587");
		element(by.id("btn-submit")).click();
		browser.sleep(3000);
		element(by.id("mat-tab-label-0-0")).click();
		browser.sleep(2000);
		element(by.id("mat-input-3")).clear().sendKeys("Sahil Thapar");
		element(by.id("mat-input-4")).clear().sendKeys("Thapar Corp.");
		element(by.id("mat-input-10")).clear().sendKeys("1234567891");
		element(by.id("mat-input-11")).clear().sendKeys("1432132132");
		element(by.id("btn-submit")).click();
		browser.sleep(3000);
		
	  });
	  */
});